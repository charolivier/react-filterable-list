import React, { Component } from 'react';

class ProductRow extends Component {
    render() {
        const product = this.props.product;
        const stocked = product.stocked;
        const warn = { color: 'red' };
        const prodName = stocked ? product.name : <span style={warn}>{ product.name }</span>;

        return (<tr>
            <td>{ prodName }</td>
            <td>{ product.price }</td>
        </tr>);
    }
}

class ProductCategoryRow extends Component {
    render() {
        const bgColor = { 'background-color': '#eee' };

        return (
            <tr>
                <th style={bgColor} colSpan="2">{this.props.category}</th>
            </tr>
        );
    }
}

class Filter extends Component {
    render() {
        return (
            <form className="jumbotron py-4">
                <div className="form-group">
                    <label htmlFor="search">Search by keyword</label>
                    <input type="text" id="search" className="form-control" onChange={this.props.updateFilter} />
                </div>
                <div className="form-group">
                    <label htmlFor="sortBy">Sort By:</label>
                    <select id="sortBy" className="form-control" onChange={this.props.updateFilter}>
                        <option value="false">None</option>
                        <option value="name">Name</option>
                        <option value="price">Price</option>
                    </select>
                </div>
                <div className="form-group mb-0">
                    <label htmlFor="stock">
                        <input type="checkbox" id="stocked" className="mr-2" onChange={this.props.updateFilter} />
                        <span>In stock Products</span>
                    </label>
                </div>
            </form>
        );
    }
}

class ProductTable extends Component {
    render() {
        const row = [];
        let lastCategory = null;
        let filteredProducts = [];

        const products = this.props.products;
        const search = this.props.filters.search.toLowerCase();
        const stocked = this.props.filters.stocked;
        const sortBy = this.props.filters.sortBy;

        // FILTER
        filteredProducts = products.filter((e) => {
            if (stocked) {
                // KEYWORD + CHECKBOX
                if (e.name.toLowerCase().indexOf(search) !== -1 && stocked === e.stocked) return true;
            } else if (e.name.toLowerCase().indexOf(search) !== -1) {
                // KEYWORD
                return true;
            }

            return false;
        });

        // SORT
        if (sortBy) {
            // DEFINE GROUPS OF ELEMENT BY CATEGORY
            const groupProducts = filteredProducts.reduce((res, elem) => {
                if (!res[elem.category]) res[elem.category] = [];

                res[elem.category].push(elem);

                return res;
            }, {});

            // SORT WITHIN EACH GROUPS
            const sortedGroups = Object.keys(groupProducts).map((key) => {
                if (sortBy === 'name') {
                    return groupProducts[key].sort((a, b) => {
                        const x = a.name.toLowerCase();
                        const y = b.name.toLowerCase();
                        if (x < y) return -1;
                        if (x > y) return 1;
                        return 0;
                    });
                }

                if (sortBy === 'price') {
                    return groupProducts[key].sort((a, b) => {
                        const x = a.price.replace('$', '');
                        const y = b.price.replace('$', '');
                        return x - y;
                    });
                }
            });

            // CONCAT
            filteredProducts = sortedGroups.reduce((res, elem) => res.concat(elem));
        }

        // MAPPING
        filteredProducts.forEach((product) => {
            if (product.category !== lastCategory) {
                row.push(<ProductCategoryRow key={product.category} category={product.category} />);
            }

            row.push(<ProductRow key={product.name} product={product} />);

            lastCategory = product.category;
        });

        const res = (row.length) ? (<table className="table table-bordered"><tbody>{row}</tbody></table>) : (<p>Nothing found..</p>);

        return (<div className="result">{res}</div>);
    }
}

class FilterableProductTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            search: '',
            stocked: false,
            sortBy: false,
        };

        this.updateFilter = this.updateFilter.bind(this);
    }

    updateFilter(e) {
        if (e.target.id === 'search') this.setState({ search: e.target.value });
        if (e.target.id === 'stocked') this.setState({ stocked: e.target.checked });
        if (e.target.id === 'sortBy') this.setState({ sortBy: e.target.value });
    }

    render() {
        return (
            <div className="filterable-product-table my-5">
                <Filter updateFilter={this.updateFilter} />

                <ProductTable products={this.props.products} filters={this.state} />
            </div>
        );
    }
}

export default FilterableProductTable;
